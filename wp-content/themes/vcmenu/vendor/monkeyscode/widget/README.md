# wpwidget
Create a backbone for widget in wordpress

## Installation
### Via composer
```bash
composer require monkeyscode/widget
```

## Usage
Create a class that extends the core class
```php
class TestWidget extends Widget
{
	public function __construct()
	{
		$widget = [
			'id' => 'test-widget',
			'label' => __('Test Widget'),
			'description' => __('This is test widget')
		];

		$fields = [
			[
		        'label' => 'Message',
		        'name' => 'message',
		        'type' => 'textarea',
			]
		];

		parent::__construct($widget, $fields);
	}

}

new TestWidget;
```

### Available Fields
See [List of fields](https://github.com/monkeyscode/wpwidget/wiki/List-of-Fields) to find out how to use these.

## Contributor
[Duy Nguyen](https://github.com/duyngha)

## Changelogs
### 1.0.6
* Add `Upload` field type

### 1.0.5
* Fix `title` display on front end form

### 1.0.4
* Fix `title` field's data

### 1.0.3
* Add `textarea` field

### 1.0.2
* Fix

### 1.0.1
* Fix

### 1.0.0
* Release first version
