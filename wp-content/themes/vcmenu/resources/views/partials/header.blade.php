<header class="banner">
  <div class="container">
    <a class="brand" href="{{ home_url('/') }}">
        {{ get_bloginfo('name', 'display') }}
    </a>
    <nav class="nav-primary">
        @if (has_nav_menu('main-menu'))
        {{-- {!! wp_nav_menu(['theme_location' => 'main-menu', 'menu_class' => 'nav']) !!} --}}
        @endif
        
        @php
        if(isset($_REQUEST['menu']) ) {
            switch ($_REQUEST['menu']) {
                case 'menu1':
                    $params = [
                        'container'       => 'ul',
                        'container_class' => 'menu1-container abc',
                        'container_id'    => 'menu1',
                        'menu_class'      => 'menu1-class class',
                        'menu_id'         => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'before'          => '',
                        'after'           => '',
                        'container_class_mobile' => ''
                    ];
                    $menu_name = 'Menu1';
                    break;
                case 'menu2':       
                    $params = [
                        'menu_id'        => 'top-menu',
                        'menu_class'     => 'nav-menu'
                    ];
                    $menu_name = 'Menu2';
                    break;       
                case 'menu3':
                    $params = [
                        'menu_id'        => 'top-menu',
                        'menu_class'     => 'nav-menu'
                    ];
                    $menu_name = 'Menu3';
                    break;
                default:
                    $params = [
                        'container'       => 'ul',
                        'container_class' => 'menu1-container abc',
                        'container_id'    => 'menu1',
                        'menu_class'      => 'menu1-class class',
                        'menu_id'         => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'before'          => '',
                        'after'           => '',
                        'container_class_mobile' => ''
                    ];
                    $menu_name = 'Menu1';
                    break;
            }
        } else {
            $params = [
                'container'       => 'ul',
                'container_class' => 'menu1-container abc',
                'container_id'    => 'menu1',
                'menu_class'      => 'menu1-class class',
                'menu_id'         => '',
                'link_before'     => '',
                'link_after'      => '',
                'before'          => '',
                'after'           => '',
                'container_class_mobile' => ''
            ];
            $menu_name = 'Menu1';
        }
        $menu = new NF\Menus\VicodersMenu($menu_name, $params);
        $menu->render();
        @endphp
    </nav>
  </div>
</header>
